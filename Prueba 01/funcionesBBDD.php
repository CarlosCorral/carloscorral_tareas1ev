<?php 
require_once "conexionPDO.php";
require_once "conexionMysqli.php";

function getUsuario($nombre,$cont) {
    try{
    $conexion=getConexionPDO();
    $usuario=null;
    $consulta = $conexion->prepare('SELECT * FROM clientes WHERE nombre=? and cont=?');
    $consulta->bindParam(1,$nombre);
    $consulta->bindParam(2,$cont);
    if($consulta->execute()){
        if($datosUsuario = $consulta->fetch()){
        $usuario=array($datosUsuario[0],$datosUsuario[1],$datosUsuario[2]);
        }
    }
    unset($conexion);
    return $usuario;
    }catch(PDOException $e){
        echo "Error";
    }
}

function getViajes(){
    try{
    $conexion=getConexionSQLi();
    $consulta = ('SELECT * from viajes');
    if($resultado=$conexion->query($consulta)){
        while ($row = $resultado->fetch_array()) {
            $viajes[]=array("id"=>$row["id"],"nombre"=>$row["nombre"],"precio"=>$row["precio"]);
        }
        $conexion->close();
        return $viajes;
    }}catch(mysqli_sql_exception $e){
        echo "Error";
    }
}

function hacerReserva($id_cliente,$id_viaje,$plazas){
    try{
    $todoOk = true;
    $conexion=getConexionPDO();
    $conexion->beginTransaction();
    $consulta =  $conexion->prepare('INSERT into reservas values(?,?,?)');
    $consulta->bindParam(1,$id_cliente);
    $consulta->bindParam(2,$id_viaje);
    $consulta->bindParam(3,$plazas);
    if ($consulta->execute() == 0) 
        $todoOk = false; 
    if ($todoOk == true){
        $conexion->commit();
        return "Se ha realizado la reserva";
        }else
        {
        $conexion->rollback();
        print "No se ha podido realizado la reserva.</p>";
        }
    unset($conexion);
    }catch(PDOException $e){
        echo "No se ha podido añadir reserva";
    }
} 
//SELECT clientes.nombre,viajes.nombre,plazas,precio FROM reservas inner join clientes on clientes.id=id_cliente inner join viajes on viajes.id=id_viaje
function getReservas(){
    try {
    $reservas=null;
    $conexion=getConexionPDO();
    $usuario=null;
    $consulta = $conexion->prepare('SELECT clientes.nombre,viajes.nombre,plazas,precio FROM reservas inner join clientes on clientes.id=id_cliente inner join viajes on viajes.id=id_viaje');
    if($consulta->execute()){
        while($reserva = $consulta->fetch()){
            $precioTotal=$reserva[2]*$reserva[3];
            $reservas[]=array($reserva[0],$reserva[1],$reserva[2],$precioTotal);
        }
    }
    unset($conexion);
    return $reservas;
    }catch(PDOException $e){
        echo "Error";
    }
}
?>