<?php 
require_once "funcionesBBDD.php";
session_start();
if($_SESSION["usuario"]==null){
    header('Location:login.php');
}else{
if(isset($_POST["reservar"])){
echo hacerReserva($_SESSION["usuario"][0],$_POST["viajes"],$_POST["numP"]);
}
?>
<!DOCTYPE html>
<html lang="es">
    <head>
    </head>
    <body>
    <h1>Reserva</h1>
    <h2><?php echo "Bienvenido ".$_SESSION["usuario"][2]?></h2>
    <form method="post">
        <select name="viajes">
        <?php 
        $viajes =getViajes();
        foreach($viajes as $viaje){
            echo "<option value='".$viaje["id"]."'";
            if(isset($_POST["reservar"])){
                echo "selected='true'";
            }
            echo ">".$viaje["nombre"]." ".$viaje["precio"]."€</option>";
        }
        ?>
        </select>
        <br>Número de personas<input type="number" name="numP" min="0">
        <br><input type="submit" name="reservar" value="Reservar">
    </form>
    <a href='reservas_realizadas.php'>Ver listado de reservas</a>
    </body>
</html>
<?php 
}?>