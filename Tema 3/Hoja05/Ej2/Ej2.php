<?php 
    $tipoMonedas=array("euros","libras","dolares");
?>
<!DOCTYPE html>
<html lang="es">
    <head>
    </head>
    <body>
    <h2>Conversor de monedas</h2>
       <form method="post">
        <input type="number" id="cantidad" name="cantidad" 
        <?php 
            if(isset($_POST["cantidad"]))
            echo "value='{$_POST['cantidad']}'";
        ?>
        />
        <select name="origen">
        <?php 
        foreach($tipoMonedas as $tipoMoneda){
            echo "<option value='$tipoMoneda'";
            if(isset($_POST["origen"])&& $tipoMoneda==$_POST["origen"])
            echo "selected='true'";
            echo">$tipoMoneda</option>";
        }
        ?>
        </select>
        <select name="destino">
        <?php
        foreach($tipoMonedas as $tipoMoneda){
            echo "<option value='$tipoMoneda'";
            if(isset($_POST["destino"])&& $tipoMoneda==$_POST["destino"])
            echo "selected='true'";
            echo">$tipoMoneda</option>";
        }
        ?>
        </select>
        <input type="submit" name="convertir" value="Convertir">
       </form>
    </body>
</html>
<?php 
if(isset($_POST["convertir"])){
    $cantidadConvertida=$_POST["cantidad"];
    $monedas=array(1,0.86,1.18);
    switch($_POST["origen"]){
        case "euros":
            $cantidadConvertida=$cantidadConvertida/$monedas[0];
            break;
        case "libras":
            $cantidadConvertida=$cantidadConvertida/$monedas[1];
            break;
        case "dolares":
            $cantidadConvertida=$cantidadConvertida/$monedas[2];
            break;
    }
    switch($_POST["destino"]){
        case "euros":
            $cantidadConvertida=$cantidadConvertida*$monedas[0];
            break;
        case "libras":
            $cantidadConvertida=$cantidadConvertida*$monedas[1];
            break;
        case "dolares":
            $cantidadConvertida=$cantidadConvertida*$monedas[2];
            break;
    }
    echo $_POST["cantidad"]." ".$_POST["origen"]." son ".$cantidadConvertida." ".$_POST["destino"] ;
}
?>