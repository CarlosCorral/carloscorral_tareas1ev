<?php 
include_once 'funcionesBBDD.php';
$equipos=getEquipos();
?>
<!DOCTYPE html>
<html lang="es">
    <head>
    </head>
    <body>
        <h2>Jugadores de la nba</h2>
       <form method="post">
        Equipo: <select name="equipos">
            <?php 
                foreach($equipos as $equipo){
                    echo "<option value='".$equipo['nombre']."'";
                    if(isset($_POST["buscar"]) && $_POST["equipos"]==$equipo){
                        echo "selected";
                    }
                    echo ">".$equipo['nombre']."</option>";
                }
            ?>
        </select>
        <input type="submit" name="buscar"value="Buscar">
        <input type="submit" name="actualizar" value="Actualizar">
        <?php 
        if(isset($_POST["buscar"])){
            $jugadores=getJugadores($_POST["equipos"]);
            echo "<table border='1px solid black'>";
            echo "<th>Nombre</th><th>Peso</th>";
            foreach($jugadores as $jugador){
                echo "<tr><td>".$jugador["nombre"]."</td><td><input type='number' name='".$jugador["codigo"]."' value='".$jugador["peso"]."'></td></tr>";
                echo "<input type='hidden' name='peso".$jugador["codigo"]."' value='".$jugador["peso"]."'>";
            }
            echo "</table>";
        }
        if(isset($_POST["actualizar"])){
            $jugadores=getJugadores($_POST["equipos"]);
            foreach($jugadores as $jugador){
                if($_POST[$jugador["codigo"]]!=$_POST["peso".$jugador["codigo"]]){
                    actualizarPeso($jugador["nombre"],$_POST[$jugador["codigo"]]);
                }   
            }
    }
        ?>
       </form>
       <a href="BajaYAlta.php">Baja y alta de jugadores</a>
    </body>
</html>