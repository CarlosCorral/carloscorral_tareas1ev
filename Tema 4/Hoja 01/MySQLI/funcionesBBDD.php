<?php 
include_once 'conexionBD.php';

function getEquipos(){
    $conexion=getConexionSQLi();
    $consulta = ('SELECT nombre from equipos');
    if($resultado=$conexion->query($consulta)){
        while ($row = $resultado->fetch_array()) {
            $aux[]=array("nombre"=>$row["nombre"]);
        }
        $conexion->close();
        return $aux;
    }
}

function getJugadores($equipo){
    $conexion=getConexionSQLi();
    $consulta=$conexion->stmt_init();
    $consulta->prepare('SELECT nombre,peso,codigo from jugadores where nombre_equipo=?');
    $consulta->bind_param("s",$equipo);
    $consulta->execute();
    $consulta->bind_result($nombre, $peso,$codigo);
        while ($consulta->fetch()) {
            $aux[]=array(
            "nombre"=>$nombre, "peso"=>$peso,"codigo"=>$codigo
        );
    }
        $conexion->close();
        return $aux;
    
}

    function getPosiciones(){
        $conexion=getConexionSQLi();
        $consulta =('SELECT distinct posicion FROM jugadores');
        if($resultado=$conexion->query($consulta)){
            while ($row = $resultado->fetch_array()) {
                $aux[]=$row["posicion"];
            }
            $conexion->close();
            return $aux;
        }
}

    function añadirYBorrar($nombreBaja,$datosJugador){
        $todoOk = true;
        $conexion=getConexionSQLi();
        $conexion->autocommit(false);
        $consulta=$conexion->prepare('DELETE from estadisticas where jugador =( select codigo from jugadores where nombre=? )');
        $consulta->bind_param('s',$nombreBaja);
        if($consulta->execute() == 0) 
        $todoOk = false; 
        $consulta =  $conexion->prepare('DELETE from jugadores where nombre=?');
        $consulta->bind_param('s',$nombreBaja);
        if ($consulta->execute() == 0) 
            $todoOk = false;
        $consulta =  $conexion->prepare('INSERT into jugadores(nombre,procedencia,altura,peso,posicion,nombre_equipo) values(?,?,?,?,?,?)');
        $consulta->bind_param('ssddss',$datosJugador["nombre"],$datosJugador["procedencia"],$datosJugador["altura"],$datosJugador["peso"],$datosJugador["posicion"],$datosJugador["nombre_equipo"]);
        if ($consulta->execute() == 0) 
            $todoOk = false; 
        if ($todoOk == true){
            $conexion->commit();
            print "<p>Los cambios se han realizado correctamente.</p>";
            }else
            {
            $conexion->rollback();
            print "<p>No se han podido realizar los cambios.</p>";
    } 
}

    function actualizarPeso($nombre,$peso){
        $conexion=getConexionSQLi();
        $consulta=$conexion->stmt_init();
        $consulta = $conexion->prepare('UPDATE jugadores set peso=? where nombre=?');
        $consulta->bind_Param('ds',$peso,$nombre);
        if($consulta->execute()){
            echo "realizado con exito";
        }else
            echo "Sin exito";
        unset($conexion);
        }

    
?>