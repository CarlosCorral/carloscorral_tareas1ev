<?php 
include "funcionesBBDD.php";
$equipos=getEquipos();
?>
<!DOCTYPE html>
<html lang="es">
    <head>
    </head>
    <body>
        <h2>Traspaso de jugadores</h2>
       <form method="post">
        Equipo: <select name="equipos">
            <?php 
                foreach($equipos as $equipo){
                    echo "<option value='".$equipo['nombre']."'";
                    if(isset($_POST["buscar"]) && $_POST["equipos"]==$equipo){
                        echo "selected";
                    }
                    echo ">".$equipo['nombre']."</option>";
                }
                
            ?>
        </select>
        <input type="submit" name="buscar"value="Buscar">
        <?php 
         if(isset($_POST["buscar"])){?>
        <h3>Alta y Baja de jugadores</h3>
        <select name="jugadores">
            <?php
            $jugadores=getJugadores($_POST["equipos"]);
            foreach($jugadores as $jugador){
                echo "<option value='".$jugador["nombre"]."'>".$jugador["nombre"]."</option>";
            }?>
            </select>
        <h3>Datos del nuevo jugador</h3>
        Nombre:<input type="text" name="nombre"><br>
        Procedencia:<input type="text" name="procedencia"><br>
        Altura:<input type="number" name="altura"><br>
        Peso:<input type="number" name="peso"><br>
        Posición<select name="posiciones"><br>
        <?php 
        $posiciones=getPosiciones();
        foreach($posiciones as $posicion){
            echo "<option value='".$posicion."'>".$posicion."</option>";
        }
        ?>
        </select><br>
        <input type="submit" name="traspaso" value="Realizar traspaso">
        <?php 
        }
        if(isset($_POST["traspaso"])){
            $datosJugador["nombre"]=$_POST["nombre"];
            $datosJugador["procedencia"]=$_POST["procedencia"];
            $datosJugador["altura"]=$_POST["altura"];
            $datosJugador["posicion"]=$_POST["posiciones"];
            $datosJugador["peso"]=$_POST["peso"];
            $datosJugador["nombre_equipo"]=$_POST["equipos"];
            $jugadorBaja=$_POST["jugadores"];
            añadirYBorrar($jugadorBaja,$datosJugador);
        }
        ?>
       </form>
      
    </body>
</html>