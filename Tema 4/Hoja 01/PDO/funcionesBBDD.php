<?php 
include_once 'conexionBD.php';

function getEquipos(){
    $conexion=getConexion();
    $consulta = $conexion->prepare('SELECT nombre from equipos');
    if($consulta->execute()){
        while ($equipo = $consulta->fetch()) {
            $equipos[]=$equipo[0];
        }
        unset($conexion);
        return $equipos;
    }
}

function getJugadores($equipo){
    $conexion=getConexion();
    $consulta = $conexion->prepare('SELECT nombre,peso,codigo from jugadores where nombre_equipo=?');
    $consulta->bindParam(1,$equipo);
    if($consulta->execute()){
        while ($jugador = $consulta->fetch()) {
            $jugadores[]=array($jugador[0],$jugador[1],$jugador[2]);
        }
        unset($conexion);
        return $jugadores;
    }
}
    function getPosiciones(){
        $conexion=getConexion();
        $consulta = $conexion->prepare('SELECT distinct posicion FROM jugadores');
        if($consulta->execute()){
            while ($posicion = $consulta->fetch()) {
                $posiciones[]=$posicion;
            }
            unset($conexion);
            return $posiciones;
        }
}

    function añadirYBorrar($nombreBaja,$datosJugador){
        $todoOk = true;
        $conexion=getConexion();
        $conexion->beginTransaction();
        $consulta =  $conexion->prepare('DELETE from estadisticas where jugador =( select codigo from jugadores where nombre=? )');
        echo $nombreBaja;
        $consulta->bindParam(1,$nombreBaja);
        if($consulta->execute() == 0) 
        $todoOk = false; 
        $consulta =  $conexion->prepare('DELETE from jugadores where nombre=?');
        $consulta->bindParam(1,$nombreBaja);
        if ($consulta->execute() == 0) 
            $todoOk = false;
        $consulta =  $conexion->prepare('INSERT into jugadores(nombre,procedencia,altura,peso,posicion,nombre_equipo) values(?,?,?,?,?,?)');
        $consulta->bindParam(1,$datosJugador["nombre"]);
        $consulta->bindParam(2,$datosJugador["procedencia"]);
        $consulta->bindParam(3,$datosJugador["altura"]);
        $consulta->bindParam(4,$datosJugador["peso"]);
        $consulta->bindParam(5,$datosJugador["posicion"]);
        $consulta->bindParam(6,$datosJugador["nombre_equipo"]);
        if ($consulta->execute() == 0) 
            $todoOk = false; 
        if ($todoOk == true){
            $conexion->commit();
            print "<p>Los cambios se han realizado correctamente.</p>";
            }else
            {
            $conexion->rollback();
            print "<p>No se han podido realizar los cambios.</p>";
    } 
}
    function actualizarPeso($nombre,$peso){
        $conexion=getConexion();
        $consulta = $conexion->prepare('UPDATE jugadores set peso=? where nombre=?');
        $consulta->bindParam(1,$peso);
        $consulta->bindParam(2,$nombre);
        if($consulta->execute()){
            echo "realizado con exito";
        }else
            echo "Sin exito";
        unset($conexion);
        }

    
?>