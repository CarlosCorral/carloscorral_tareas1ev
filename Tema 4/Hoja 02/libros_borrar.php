<!DOCTYPE html>
<html lang="es">
    <head>
    </head>
    <body>
        <form method="post">
           Titulo: <select name="nombreLibro">
            <?php 
                require_once "funcionesBaseDatos.php";
                $libros=getLibros();
                foreach($libros as $libro){
                    echo "<option value='".$libro["titulo"]."'>".$libro['titulo']."</option>";
                }
            ?>
            </select>
            <input type="submit" name="borrar" value="Borrar">
        </form>
        <?php 
        if(isset($_POST["borrar"])){
            $nombreLibro=$_POST["nombreLibro"];
            $precio=borrarLibro($nombreLibro);
            if($precio!=null){
                echo "El precio del libro borrado era de". $precio[0]."€";
            }else
            echo "No se ha podido borrar";
        }
        ?>
    </body>
</html>