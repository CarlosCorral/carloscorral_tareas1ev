<?php 
require_once "conexionBD.php";
require_once "conexionBDMysqli.php";
function guardarLibro($titulo,$anyo,$precio,$fecha){
    $conexion=getConexion();
    $consulta=$conexion->prepare('insert into libros(titulo,precio,edicion,adquisicion) values(?,?,?,?)');
    $consulta->bindParam(1,$titulo);
    $consulta->bindParam(2,$precio);
    $consulta->bindParam(3,$anyo);
    $consulta->bindParam(4,$fecha);
    if($consulta->execute()){
        unset($conexion);
        return "Guardado correctamente";
    }
    unset($conexion);
    return "No se ha podido guardar";
}
//MYSQLI
function getLibros() {
    $conexion= getConexionSQLi();
    $sql="select * from libros";
    if ($resultado=$conexion->query($sql)) {
        while ($row = $resultado->fetch_array()) {
            $libros[]=array(
                "titulo"=>$row["titulo"],
                "precio"=>$row["precio"],
                "edicion"=>$row["edicion"],
                "adquisicion"=>$row["adquisicion"] );
        }
        
    }
    $conexion->close();
    return $libros;
}


function borrarLibro($titulo){
    $todoOk = true;
    $conexion=getConexion();
    $conexion->beginTransaction();
    $consulta = $conexion->prepare('SELECT precio FROM libros where titulo = ?');
    $consulta->bindParam(1,$titulo);
        if($consulta->execute()){
            $precio = $consulta->fetch();
        }else
            $todoOk=false; 
    $consulta=$conexion->prepare('DELETE from libros where titulo = ? ');
    $consulta->bindParam(1,$titulo);
    if ($consulta->execute() == 0) 
        $todoOk = false;
    if ($todoOk == true){
        $conexion->commit();
        return $precio;
        }else{
        $conexion->rollback();
        print "<p>No se han podido realizar los cambios.</p>";
        return null;
} 
}
?>