<?php 
require_once "conexionBD.php";

function getPlazas() {
    $conexion=getConexion();
    $consulta = $conexion->prepare('SELECT * from plazas where reservada=0');
    if($consulta->execute()){
        while ($plaza = $consulta->fetch()) {
            $plazas[]=array($plaza[0],$plaza[1],$plaza[2]);
        }
        unset($conexion);
        return $plazas;
    }
}

function añadirPasajero($nombre,$dni,$plaza){
    $todoOk = true;
    $conexion=getConexion();
    $conexion->beginTransaction();
    $consulta =  $conexion->prepare("INSERT into pasajeros values(?,?,'-',?)");
    $consulta->bindParam(1,$nombre);
    $consulta->bindParam(2,$dni);
    $consulta->bindParam(3,$plaza);
    if($consulta->execute() == 0) 
    $todoOk = false; 
    $consulta =  $conexion->prepare('UPDATE plazas SET reservada=1 WHERE numero=?');
    $consulta->bindParam(1,$plaza);
    if($consulta->execute() == 0) 
    $todoOk = false; 
    if ($todoOk == true){
        $conexion->commit();
        print "<p>Se ha reserrvado las plaza $plaza</p>";
        }else
        {
        $conexion->rollback();
        print "<p>No se han podido realizar los cambios.</p>";
    }  
}
function llegada(){
    $todoOk = true;
    $conexion=getConexion();
    $conexion->beginTransaction();
    $consulta =  $conexion->prepare("delete from pasajeros");
    if($consulta->execute() == 0) 
    $todoOk = false; 
    $consulta =  $conexion->prepare('UPDATE plazas SET reservada=0 WHERE reservada=1');
    if($consulta->execute() == 0) 
    $todoOk = false; 
    if ($todoOk == true){
        $conexion->commit();
        print "<p>Los cambios se han realizado correctamente.</p>";
        }else
        {
        $conexion->rollback();
        print "<p>No se han podido realizar los cambios.</p>";
    }
}
?>