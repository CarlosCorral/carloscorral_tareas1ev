<?php 
require_once "conexionBD.php";

function getPlazas() {
    $conexion=getConexionSQLi();
    $consulta =('SELECT * from plazas where reservada=0');
    if($resultado=$conexion->query($consulta)){
        while ($row = $resultado->fetch_array()) {
            $plazas[]=array("numero"=>$row["numero"],"reservada"=>$row["reservada"],"precio"=>$row["precio"]);
        }
        unset($conexion);
        return $plazas;
    }
}

function añadirPasajero($nombre,$dni,$plaza){
    $todoOk = true;
    $conexion=getConexionSQLi();
    $conexion->autocommit(false);
    $consulta=$conexion->stmt_init();
    $consulta =  $conexion->prepare("INSERT into pasajeros values(?,?,'-',?)");
    $consulta->bind_Param("ssd",$nombre,$dni,$plaza);
    if($consulta->execute() == 0) 
    $todoOk = false; 
    $consulta =  $conexion->prepare('UPDATE plazas SET reservada=1 WHERE numero=?');
    $consulta->bind_Param('d',$plaza);
    if($consulta->execute() == 0) 
    $todoOk = false; 
    if ($todoOk == true){
        $conexion->commit();
        print "<p>Se ha reserrvado las plaza $plaza</p>";
        }else
        {
        $conexion->rollback();
        print "<p>No se han podido realizar los cambios.</p>";
    }  
}

function llegada(){
    $todoOk = true;
    $conexion=getConexionSQLi();
    $conexion->autocommit(false);
    $consulta =  $conexion->prepare("delete from pasajeros");
    if($consulta->execute() == 0) 
    $todoOk = false; 
    $consulta =  $conexion->prepare('UPDATE plazas SET reservada=0 WHERE reservada=1');
    if($consulta->execute() == 0) 
    $todoOk = false; 
    if ($todoOk == true){
        $conexion->commit();
        print "<p>Los cambios se han realizado correctamente.</p>";
        }else
        {
        $conexion->rollback();
        print "<p>No se han podido realizar los cambios.</p>";
    }
}
?>